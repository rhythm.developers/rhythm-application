import React, { useState, useEffect } from "react";
import "../../styles/pages/Home.scss";

const Home = () => {
  const [btnEnabled, setBtnEnabled] = useState(false);
  const [email, setEmail] = useState("");

  function handleForm(event) {
    event.preventDefault();
  }

  const handleEmail = (event) => {
    setEmail(event.target.value);
  };

  return (
    <div className="landing-page">
      <div className="container">
        <div className="content">
          <img className="app-symbol" src="./images/rhythm-symbol.png" />
          <h2 className="branding">Rhythm</h2>
          <p className="tagline">CREATE | SHARE | GROOVE</p>
          <div className="signup-early-access">
            <form id="form" onSubmit={handleForm}>
              <input
                type="email"
                maxLength={100}
                id="email"
                name="email"
                value={email}
                placeholder="Drop the Mic, Enter your Email"
                onChange={handleEmail}
                pattern="[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$"
              />
              <input id="submit" type="submit" />
            </form>
          </div>
        </div>
        <div className="banner-image-container-first">
          <img className="banner-image" src="./images/banner.png" />
        </div>
      </div>
    </div>
  );
};

export default Home;
